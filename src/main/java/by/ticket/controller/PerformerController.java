package by.ticket.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import by.ticket.model.Performer;
import by.ticket.service.PerformerService;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:23
 */
@ManagedBean(name="performerBean")
@SessionScoped
public class PerformerController {
	private Performer performer;

	@ManagedProperty(value="#{performerService}")
	private PerformerService performerService;

	public PerformerController() {

	}
	
	public void addPerformer(){
		boolean isSaved = performerService.savePerformer(performer);
		String msg = isSaved ? "Saved!" : "NOT saved. Title is not unique.";
		FacesContext f = FacesContext.getCurrentInstance();
		f.addMessage("messages", new FacesMessage(msg));
	}

	
	public String toPerformer(String id) {
		performer = performerService.getPerformer(id);
		return "/pages/unsecure/performer";
	}
	
	public String toAddPerformer() {
		performer = new Performer();
		return "/pages/admin/editPerformer";
	}
	
	public String toEditPerformer(String id) {
		performer = performerService.getPerformer(id);
		return "/pages/admin/editPerformer";
	}

	public void setPerformerService(PerformerService performerService){
		this.performerService = performerService;
	}

	public Performer getPerformer(String id) {
		return performerService.getPerformer(id);
	}
	
	public Performer getPerformerByTitle(String title) {
		return performerService.getPerformerByTitle(title);
	}

	public List<Performer> getPerformerList(){
		return performerService.getPerformerList();
	}

	public void updatePerformer(Performer performer){

	}

	public Performer getPerformer() {
		return performer;
	}

	public void setPerformer(Performer performer) {
		this.performer = performer;
	}
}