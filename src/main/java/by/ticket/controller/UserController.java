package by.ticket.controller;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;
import java.util.Collections;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import by.ticket.service.UserService;
import by.ticket.util.Utils;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
@ManagedBean(name="userBean")
@SessionScoped
public class UserController implements Serializable {
	private static final long serialVersionUID = -8118462113323863022L;
	private static final Logger LOG = Logger.getLogger(UserController.class);
	private String password;
	private String username;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	@ManagedProperty(value="#{authenticationManager}")
    private AuthenticationManager authenticationManager;

	public UserController() {
		
	}
	
	public String login(){
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(
            		this.getUsername(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
            if (result.isAuthenticated() && Utils.hasRole(result.getAuthorities(), "ROLE_ADMIN")) {
            	return "/pages/admin/admin";
            }
        } catch (AuthenticationException e) {
        	LOG.info("Authentivation failed. " + e.getMessage());
        	FacesContext.getCurrentInstance().addMessage(null, 
        			new FacesMessage(FacesMessage.SEVERITY_ERROR,"Wrong credentials", "Username or password was incorrect"));  
        	return "/pages/home/home";
        }
        return "/pages/unsecure/concerts";
	}

	public void register(){
		boolean usernameNotBlank = isNotBlank(username);
		boolean passwordNotBlank = isNotBlank(password);
		if (usernameNotBlank && passwordNotBlank) {	
			userService.register(username, password, Collections.<GrantedAuthority>emptySet());
			FacesContext.getCurrentInstance().addMessage("username", 
        			new FacesMessage(FacesMessage.SEVERITY_INFO,"Success", "You are registered. Use login page next"));
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			if (!usernameNotBlank) {
				context.addMessage("username", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Username cannot be blank"));
			}
			if (!passwordNotBlank) {
				context.addMessage("password", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Password cannot be blank"));
			}
			FacesContext.getCurrentInstance().addMessage(null, 
        			new FacesMessage(FacesMessage.SEVERITY_ERROR,"Credentials required", "Username and password are required"));
		}
	}
	
	public boolean checkRole(String role) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal.getClass().equals(String.class)) {
			// if "anonimousUser" logged in
			String anonimousUser = (String) principal;
			return StringUtils.equals(anonimousUser, role);
		}
		UserDetails  user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		for (GrantedAuthority userRole : user.getAuthorities()) {
			if (userRole.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}
	
	public void logout(){

	}

	public void ban(){

	}
	
	public void closeAccount(){

	}

	public void deleteUser(){

	}
	
	public String getPassword(){
		return this.password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getUsername(){
		return this.username;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public void setUserService(UserService userService){
		this.userService = userService;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
}