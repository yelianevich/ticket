package by.ticket.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;

import by.ticket.model.Performer;
import by.ticket.service.PerformerService;

@ManagedBean(name = "performerConverter")
@SessionScoped
public class PerformerConverter implements Converter {
	
	@ManagedProperty(value = "performerService")
	private PerformerService performerService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String title) {
		if (StringUtils.isBlank(title)) {  
            return null;  
        } else {  
            return performerService.getPerformerByTitle(title); 
        }
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		 if (value == null || value.equals("")) {  
	            return StringUtils.EMPTY;  
	        } else {  
	            return String.valueOf(((Performer) value).getTitle());  
	        }  
	}

	public void setPerformerService(PerformerService performerService) {
		this.performerService = performerService;
	}
}
