package by.ticket.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.UnselectEvent;

import by.ticket.model.Concert;
import by.ticket.model.Performer;
import by.ticket.service.ConcertService;
import by.ticket.service.PerformerService;



/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:22
 */
@ViewScoped
@ManagedBean(name="concertBean")
@SessionScoped
public class ConcertController {
	private Concert concert = new Concert();
	private List<Concert> concerts;
	private String title = "";
	private List<Performer> performers = new ArrayList<>();
    private List<Performer> selectedPerformers = new ArrayList<>();  
    private List<String> selectedTexts = new ArrayList<>();
    
	@ManagedProperty(value = "performerService")
	private PerformerService performerService;
	
	@ManagedProperty(value="#{concertService}")
	private ConcertService concertService;

	public ConcertController(){

	}

	public String searchConcerts(){
		concerts = concertService.searchConcerts(title);
		return "/pages/unsecure/concertsSearch";
	}
	
	public String toAddConcert() {
		this.concert = new Concert();
		selectedPerformers = new ArrayList<>();
		return "/pages/admin/editConcert";
	}
	
	public String toEditConcert(String id) {
		this.concert = concertService.getConcert(id);
		selectedPerformers = concert.getPerformerList();
		return "/pages/admin/editConcert";
	}

	public List<Concert> getConcertList() {
		return concertService.getConcertList();
	}

	public void updateConcert(){

	}
	
	public List<Performer> completePerformer(String titleQuery) {  
        List<Performer> suggestions = performerService.searchPerformers(titleQuery, 10);
        return suggestions;  
    }
	
    public void handleUnselect(UnselectEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected:" + event.getObject().toString(), null);  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }

	public void addConcert() {
		concert.setPerformerList(selectedPerformers);
		boolean isSaved = concertService.saveConcert(concert);
		String msg = isSaved ? "Saved!" : "Title is not unique.";
		FacesContext f = FacesContext.getCurrentInstance();
		f.addMessage("messages", new FacesMessage(msg));
	}
	
	public List<Concert> getConcerts() {
		return concerts;
	}

	public void setConcerts(List<Concert> concerts) {
		this.concerts = concerts;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Concert getConcert() {
		return concert;
	}

	public void setConcert(Concert concert) {
		this.concert = concert;
	}
	

	public void setConcertService(ConcertService concertService){
		this.concertService = concertService;
	}

	public List<Performer> getPerformers() {
		return performers;
	}

	public void setPerformers(List<Performer> performers) {
		this.performers = performers;
	}

	public List<Performer> getSelectedPerformers() {
		return selectedPerformers;
	}

	public void setSelectedPerformers(List<Performer> selectedPerformers) {
		this.selectedPerformers = selectedPerformers;
	}

	public List<String> getSelectedTexts() {
		return selectedTexts;
	}

	public void setSelectedTexts(List<String> selectedTexts) {
		this.selectedTexts = selectedTexts;
	}

	public void setPerformerService(PerformerService performerService) {
		this.performerService = performerService;
	}

}