package by.ticket.dao;

import by.ticket.model.ShopUser;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public interface IUserDao {

	/**
	 * 
	 * @param username
	 */
	public ShopUser getUser(String username);

	/**
	 * 
	 * @param shopUser
	 */
	public void updateUser(ShopUser shopUser);

	/**
	 * 
	 * @param user
	 */
	public boolean addUser(ShopUser user);

	/**
	 * 
	 * @param user
	 */
	public void deleteUser(String username);

}