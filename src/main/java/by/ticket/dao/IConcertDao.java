package by.ticket.dao;

import java.util.List;

import by.ticket.model.Concert;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public interface IConcertDao {

	public Concert getConcertByTitle(String title);
	
	public Concert getConcert(String id);
	
	public void deleteConcert(String id);
	
	public void deleteConcertByTitle(String title);

	public List<Concert> getConcertList();
	
	public List<Concert> searchConcertList(String title, int limit);

	public boolean updateConcert(Concert concert);

	public boolean addConcert(Concert concert);
	
	

}