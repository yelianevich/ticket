package by.ticket.dao;

import java.util.List;

import by.ticket.model.Performer;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public interface IPerformerDao {

	Performer getPerformer(String id);
	
	Performer getPerformerByTitle(String title);

	List<Performer> getPerformerList();

	boolean addPerformer(Performer performer);

	boolean updatePerformer(Performer performer);
	
	void deletePerformer(String id);

	void deletePerformerByTitle(String title);

	List<Performer> searchPerformer(String titleQuery, int limit);

}