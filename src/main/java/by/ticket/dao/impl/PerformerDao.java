package by.ticket.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import by.ticket.dao.IPerformerDao;
import by.ticket.model.Performer;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public class PerformerDao implements IPerformerDao {
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private DBCollection performersCollection;
	
	public PerformerDao(){

	}
	
	@Override
	public Performer getPerformer(String id){
		DBObject performerDoc = performersCollection.findOne(new BasicDBObject("_id", new ObjectId(id)));
		return getPerformer(performerDoc);
	}

	@Override
	public List<Performer> getPerformerList() {
		int limit = 200;
		DBCursor cursor = performersCollection.find().limit(limit);
		List<Performer> performerList = getPerformers(cursor, limit);
		return performerList;
	}

	@Override
	public boolean addPerformer(Performer performer){
		DBObject doc = getDocument(performer);
		try {
			performersCollection.insert(doc);
		} catch (MongoException.DuplicateKey e) {
			LOG.info("Key is duplecated, document is rejected. " + e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean updatePerformer(Performer performer){
		DBObject performerDoc = getDocument(performer);
		try {	
			performersCollection.update(new BasicDBObject("_id", performerDoc.get("_id")), performerDoc);
		} catch (MongoException.DuplicateKey e) {
			LOG.info(e.getMessage(), e);
			return false;
		}
		return true;
	}
	
	@Override
	public void deletePerformer(String id) {
		performersCollection.remove(new BasicDBObject("_id", new ObjectId(id)));
	}
	
	@Override
	public void deletePerformerByTitle(String title) {
		performersCollection.remove(new BasicDBObject("title", title));
	}
	
	@Override
	public Performer getPerformerByTitle(String title) {
		DBObject performerDoc = performersCollection.findOne(new BasicDBObject("title", title));
		return getPerformer(performerDoc);
	}

	@Override
	public List<Performer> searchPerformer(String titleQuery, int limit) {
		BasicDBObject query = new BasicDBObject("title", Pattern.compile(titleQuery));
		DBCursor cursor = performersCollection.find(query).limit(limit);
		List<Performer> performerList = getPerformers(cursor, limit);
		return performerList;
	}
	
	public void setPerformersCollection(DBCollection performersCollection) {
		this.performersCollection = performersCollection;
	}
	
	private DBObject getDocument(Performer performer) {
		DBObject performerDoc = new BasicDBObject("title", performer.getTitle())
									.append("description", performer.getDescription())
									.append("style", performer.getStyle());
		if (StringUtils.isNotBlank(performer.getId())) {
			performerDoc.put("_id", new ObjectId(performer.getId()));
		}
		return performerDoc;
	}
	
	private List<Performer> getPerformers(DBCursor performerCursor, int limit) {
		List<Performer> performerList = new ArrayList<>();
		try {
			List<DBObject> performerDocs = performerCursor.toArray();
			for (DBObject performerDoc : performerDocs) {
				performerList.add(getPerformer(performerDoc));
			}
		} catch (MongoException e) {
			LOG.error(e.getMessage());
		}
		return performerList;
	}
	
	private Performer getPerformer(DBObject performerDoc) {
		Performer performer = new Performer();
		performer.setId(performerDoc.get("_id").toString());
		performer.setTitle(performerDoc.get("title").toString());
		performer.setDescription(performerDoc.get("description").toString());
		performer.setStyle(performerDoc.get("style").toString());
		return performer;
	}
}