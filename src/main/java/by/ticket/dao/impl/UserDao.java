package by.ticket.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.ticket.dao.IUserDao;
import by.ticket.model.ShopUser;
import by.ticket.util.Utils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
public class UserDao implements IUserDao {
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private DBCollection usersCollection;
	
	public UserDao() {

	}
	
	public ShopUser getUser(String username){
		ShopUser shopUser = null;
		DBObject user = usersCollection.findOne(new BasicDBObject("_id", username));
		if (user != null) {	
			Set<GrantedAuthority> authorities = new HashSet<>();
			BasicDBList roles = (BasicDBList) user.get("roles");
			for(Object role : roles) {
				authorities.add(new SimpleGrantedAuthority(role.toString()));
			}
			shopUser = new ShopUser(
					user.get("_id").toString(), 
					user.get("password").toString(), 
					authorities);
		}
		return shopUser;
	}

	public void updateUser(ShopUser shopUser){
		
	}

	public boolean addUser(ShopUser shopUser){
		DBObject user = new BasicDBObject("_id", shopUser.getUsername())
						.append("password", shopUser.getPassword())
						.append("roles", Utils.getRoles(shopUser.getAuthorities()));
		try {
			usersCollection.insert(user);
            return true;
        } catch (MongoException.DuplicateKey e) {
            LOG.info("Username already in use: " + shopUser.getUsername());
            return false;
        }
	}

	public void deleteUser(String username){
		usersCollection.remove(new BasicDBObject("_id", username));
	}
	
	public void setUsersCollection(DBCollection usersCollection) {
		this.usersCollection = usersCollection;
	}
}