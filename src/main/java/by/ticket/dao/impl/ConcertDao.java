package by.ticket.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import by.ticket.dao.IConcertDao;
import by.ticket.dao.IPerformerDao;
import by.ticket.model.Concert;
import by.ticket.model.Performer;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public class ConcertDao implements IConcertDao {
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private DBCollection concertsCollection;
	private IPerformerDao performerDao;
	
	public ConcertDao() {
	}
	
	@Override
	public Concert getConcertByTitle(String title) {
		Concert concert = null;
		
		DBObject concertDoc = concertsCollection.findOne(new BasicDBObject("title", title));
		if (concertDoc != null) {	
			concert = getConcert(concertDoc);
		}
		return concert;
	}
	
	@Override
	public Concert getConcert(String id){
		ObjectId objectId = new ObjectId(id);
		DBObject concertDoc = concertsCollection.findOne(new BasicDBObject("_id", objectId));
		Concert concert = getConcert(concertDoc);
		return concert;
	}

	@Override
	public List<Concert> getConcertList(){
		DBCursor cursor = concertsCollection.find();
		List<Concert> concerts = getConcertsFromCursor(cursor);
		return concerts;
	}

	@Override
	public boolean updateConcert(Concert concert){
		DBObject concertDoc = getDocument(concert);
		DBObject query = new BasicDBObject("_id", concertDoc.get("_id"));
		try {
			concertsCollection.update(query, concertDoc);
		} catch (MongoException.DuplicateKey e) {
			LOG.info(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public boolean addConcert(Concert concert){
		DBObject concertDoc = getDocument(concert);
		try {
			concertsCollection.insert(concertDoc);
		} catch (MongoException.DuplicateKey e) {
			LOG.info(e.getMessage());
			return false;
		}
		return true;
	}
	
	public void setConcertsCollection(DBCollection concertsCollection) {
		this.concertsCollection = concertsCollection;
	}
	
	@Override
	public void deleteConcert(String id) {
		ObjectId objectId = new ObjectId(id);
		concertsCollection.remove(new BasicDBObject("_id", objectId));
	}

	@Override
	public void deleteConcertByTitle(String title) {
		concertsCollection.remove(new BasicDBObject("title", title));
	}
	
	private Concert getConcert(DBObject concertDoc) {
		Concert concert = new Concert();
		concert.setId(concertDoc.get("_id").toString());
		concert.setTitle(concertDoc.get("title").toString());
		concert.setDescription(concertDoc.get("description").toString());
		concert.setAddress(concertDoc.get("address").toString());
		
		@SuppressWarnings("unchecked")
		List<ObjectId> performerIds = (List<ObjectId>) concertDoc.get("performers");
		List<Performer> performers = new ArrayList<>(performerIds.size());
		for (ObjectId id : performerIds) {
			Performer performer = performerDao.getPerformer(id.toString());
			performers.add(performer);
		}
		concert.setPerformerList(performers);
		return concert;
	}
	
	private DBObject getDocument(Concert concert) {
		DBObject concertDoc = new BasicDBObject("title", concert.getTitle())
			.append("address", concert.getAddress())
			.append("description", concert.getDescription());
		if (StringUtils.isNotBlank(concert.getId())) {
			concertDoc.put("_id", new ObjectId(concert.getId()));
		}
		List<ObjectId> performerIds = getPerformersIds(concert.getPerformerList());
		concertDoc.put("performers", performerIds);
		return concertDoc;
	}
	

	public void setPerformerDao(IPerformerDao performerDao) {
		this.performerDao = performerDao;
	}
	
	private List<ObjectId> getPerformersIds(List<Performer> performers) {
		List<ObjectId> performerIds = new ArrayList<>(performers.size());
		for (Performer performer : performers) {
			ObjectId id = new ObjectId(performer.getId());
			performerIds.add(id);
		}
		return performerIds;
	}

	@Override
	public List<Concert> searchConcertList(String title, int limit) {
		BasicDBObject query = new BasicDBObject();
		
		query.put("title", Pattern.compile(title));
		DBCursor cursor = concertsCollection.find(query).limit(limit);
		List<Concert> concerts = getConcertsFromCursor(cursor);
		return concerts;
	}
	
	private List<Concert> getConcertsFromCursor(DBCursor cursor) {
		List<Concert> concerts = new ArrayList<>();
		try {
			List<DBObject> concertDocs = cursor.toArray();
			for (DBObject concertDoc : concertDocs) {
				Concert concert = getConcert(concertDoc);
				concerts.add(concert);
			}
		} catch (MongoException e) {
			LOG.error(e.getMessage());
		}
		return concerts;
	}
}