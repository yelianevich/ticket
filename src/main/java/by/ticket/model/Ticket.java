package by.ticket.model;

import java.math.BigDecimal;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
public class Ticket {

	private Concert concert;
	private BigDecimal price;

	public Ticket(){

	}
	public Concert getConcert(){
		return concert;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setConcert(Concert newVal){
		concert = newVal;
	}

	public BigDecimal getPrice(){
		return price;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setPrice(BigDecimal newVal){
		price = newVal;
	}
}