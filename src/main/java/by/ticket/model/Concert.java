package by.ticket.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
public class Concert implements Serializable {
	private static final long serialVersionUID = -7902601202113763230L;
	
	private String id;
	private String title;
	private String description;
	private String address;
	private List<Performer> performerList;
	private List<Ticket> ticketList;

	public Concert(){
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress(){
		return address;
	}

	public void setAddress(String newVal){
		address = newVal;
	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String newVal){
		description = newVal;
	}

	public String getTitle(){
		return title;
	}


	public void setTitle(String newVal){
		title = newVal;
	}
	
	public List<Performer> getPerformerList() {
		return performerList;
	}
	
	public void setPerformerList(List<Performer> performerList) {
		this.performerList = performerList;
	}
	
	public List<Ticket> getTicketList() {
		return ticketList;
	}
	
	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}
}