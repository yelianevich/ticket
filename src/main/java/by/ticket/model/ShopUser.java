package by.ticket.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
public class ShopUser extends User {
	private static final long serialVersionUID = 809204807080927669L;
	private Basket basket;

	public ShopUser(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public Basket getBasket(){
		return basket;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setBasket(Basket newVal){
		basket = newVal;
	}
}