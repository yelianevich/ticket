package by.ticket.model;

import java.io.Serializable;


/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:04
 */
public class Performer implements Serializable {
	private static final long serialVersionUID = -7791978188890866180L;
	private String id;
	private String title;
	private String description;
	private String style;

	public Performer() {

	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String newVal){
		description = newVal;
	}

	public String getStyle(){
		return style;
	}

	public void setStyle(String newVal){
		style = newVal;
	}

	public String getTitle(){
		return title;
	}

	public void setTitle(String newVal){
		title = newVal;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return title;
	}
}