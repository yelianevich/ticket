package by.ticket.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import by.ticket.dao.IPerformerDao;
import by.ticket.model.Performer;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public class PerformerService {
	private IPerformerDao performerDao;

	public PerformerService(){

	}

	public Performer getPerformerByTitle(String title){
		return performerDao.getPerformerByTitle(title);
	}
	
	public Performer getPerformer(String id){
		return performerDao.getPerformer(id);
	}

	public List<Performer> getPerformerList(){
		return performerDao.getPerformerList();
	}

	public boolean savePerformer(Performer performer){
		if (StringUtils.isBlank(performer.getId())) {
			return performerDao.addPerformer(performer);
		} else {
			return performerDao.updatePerformer(performer);
		}
	}

	public void setPerformerDao(IPerformerDao performerDao){
		this.performerDao = performerDao;
	}

	public List<Performer> searchPerformers(String titleQuery, int limit) {
		return performerDao.searchPerformer(titleQuery, limit);
	}
}