package by.ticket.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import by.ticket.dao.IUserDao;

/**
 * Custom implementation of UserDetailsService to get
 * datails about user.
 * @author Raman Yelianevich
 *
 */
public class ShopUserDetailService implements UserDetailsService {
	private IUserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails userDetails = userDao.getUser(username);
		return userDetails;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
}
