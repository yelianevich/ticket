package by.ticket.service;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.ticket.dao.IUserDao;
import by.ticket.model.ShopUser;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public class UserService {
	private IUserDao userDao;
	private static final Set<GrantedAuthority> USER_ROLE = new HashSet<>();
	private static final Set<GrantedAuthority> ADMIN_ROLE = new HashSet<>();
	
	static {
		USER_ROLE.add(new SimpleGrantedAuthority("ROLE_USER"));
		ADMIN_ROLE.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		ADMIN_ROLE.add(new SimpleGrantedAuthority("ROLE_USER"));
	}

	public UserService(){

	}

	public boolean checkCredentials(String username, String password){
		ShopUser user = userDao.getUser(username);
		if (user != null) {
			if (StringUtils.equals(username, password)) {
				return true;
			}
		}
		return false;
	}

	public boolean register(String username, String password, Set<GrantedAuthority> roles){
		assert roles != null;
		
		ShopUser user = new ShopUser(username, password, roles.isEmpty() ? USER_ROLE : roles);
		return userDao.addUser(user);
	}

	public void ban(ShopUser user){

	}

	public void closeAccount(ShopUser user){

	}

	public void deleteUser(String username){
		userDao.deleteUser(username);
	}

	public void setUserDao(IUserDao userDao){
		this.userDao = userDao;
	}
}