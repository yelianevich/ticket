package by.ticket.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import by.ticket.dao.IConcertDao;
import by.ticket.model.Concert;

/**
 * @author Raman
 * @version 1.0
 * @created 09-Nov-2013 21:46:05
 */
public class ConcertService {
	private IConcertDao concertDao;

	public ConcertService(){

	}

	public Concert getConcert(String id){
		return concertDao.getConcert(id);
	}
	
	public Concert getConcertByTitle(String concertTitle){
		return concertDao.getConcertByTitle(concertTitle);
	}

	public List<Concert> getConcertList(){
		return concertDao.getConcertList();
	}
	
	public List<Concert> searchConcerts(String title){
		return concertDao.searchConcertList(title, 10);
	}

	public void updateConcert(Concert concert){
		concertDao.updateConcert(concert);
	}

	public boolean saveConcert(Concert concert){
		if (StringUtils.isBlank(concert.getId())) {
			return concertDao.addConcert(concert);
		} else {
			return concertDao.updateConcert(concert);
		}
		
	}

	public void setConcertDao(IConcertDao concertDao){
		this.concertDao = concertDao;
	}
}