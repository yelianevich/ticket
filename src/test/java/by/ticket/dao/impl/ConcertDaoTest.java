package by.ticket.dao.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.ticket.dao.IConcertDao;
import by.ticket.dao.IPerformerDao;
import by.ticket.model.Concert;
import by.ticket.model.Performer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/configuration/spring/beans/app_context.xml"})
public final class ConcertDaoTest {
	private IConcertDao concertDao;
	private IPerformerDao performerDao;
	
	@Autowired
	public void setPerformerDao(IPerformerDao performerDao) {
		this.performerDao = performerDao;
	}

	@Autowired
	public void setConcertDao(IConcertDao concertDao) {
		this.concertDao = concertDao;
	}

	@Before
	public void shouldInsertConcert() {
		Performer performer = new Performer();
		performer.setTitle("Performer1");
		performer.setDescription("Description1");
		performer.setStyle("style1");
		performerDao.addPerformer(performer);
		Performer perf = performerDao.getPerformerByTitle("Performer1");
		Concert concert = new Concert();
		concert.setTitle("SuperTestConcert");
		concert.setAddress("SuperTestConcertAddress");
		concert.setDescription("SuperTestConcertDescr");
		List<Performer> l = new ArrayList<>();
		l.add(perf);
		concert.setPerformerList(l);
		concertDao.addConcert(concert);
	}

	@Test
	public void shouldReturnNotNullConcert() {
		Concert concert = concertDao.getConcertByTitle("SuperTestConcert");
		assertThat(concert, is(notNullValue()));
	}
	
	@Test
	public void shouldReturnSomeConcerts() {
		List<Concert> concerts = concertDao.getConcertList();
		assertThat(concerts, is(not(empty())));
	}
	
	@After
	public void removeConcert() {
		concertDao.deleteConcertByTitle("SuperTestConcert");
		performerDao.deletePerformerByTitle("Performer1");
	}
}