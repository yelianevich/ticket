package by.ticket.dao.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.ticket.dao.IPerformerDao;
import by.ticket.model.Performer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/configuration/spring/beans/app_context.xml"})
public final class PerformerDaoTest {
	private IPerformerDao performerDao;
	
	@Autowired
	public void setPerformerDao(IPerformerDao performerDao) {
		this.performerDao = performerDao;
	}

	@Before
	public void insertPerformer() {
		Performer performer = new Performer();
		performer.setTitle("Performer1");
		performer.setDescription("Description1");
		performer.setStyle("style1");
		if (performerDao.addPerformer(performer)) {
			System.out.println("Performer Inserted");
		} else {
			System.out.println("Performer exists. Not inserted");
		}
	}

	@Test
	public void shouldReturnNotNullUser() {
		Performer performer = performerDao.getPerformerByTitle("Performer1");
		assertThat(performer.getTitle(), is("Performer1"));
	}
	
	@Test
	public void shouldReturnSomePerformers() {
		List<Performer> performers = performerDao.getPerformerList();
		assertThat(performers, is(not(empty())));
	}
	
	@After
	public void removeConcert() {
		performerDao.deletePerformer("Performer1");
	}
}