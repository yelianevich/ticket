package by.ticket.dao.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.ticket.dao.IUserDao;
import by.ticket.model.ShopUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/configuration/spring/beans/app_context.xml"})
public final class UserDaoTest {
	private IUserDao userDao;
	
	@Autowired
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	
	@Before
	public void insertUser() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		ShopUser shopUser = new ShopUser(
				"username",
				"password", 
				authorities);
		userDao.addUser(shopUser);
	}

	@Test
	public void shouldReturnNotNullUser() {
		ShopUser user = userDao.getUser("username");
		assertThat(user, is(notNullValue()));
	}
	
	@After
	public void removeUser() {
		userDao.deleteUser("username");
	}
}