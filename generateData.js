use ticketmaster;
db.concerts.drop();
db.performers.drop();
for (var i = 0; i < 100; ++i) {
	var longPerformerDesc = "";
	for (var j = 0; j < 50;  ++j) {
		longPerformerDesc += "PerformerDescription ";
	}
	var performer = {
		"title" : "PerformerTitle" + i,
		"description" : longPerformerDesc + i,
		"style" : "PerformerStyle" + i
	};
	db.performers.insert(performer);
	
	var longConcertDesc = "";
	for (var q = 0; q < 50;  ++q) {
		longConcertDesc += "ConcertDescription ";
	}
	
	var perfWithId = db.performers.findOne(performer); 
	var concert = {
		"title" : "ConcertTitle" + i,
		"description" : longConcertDesc + i,
		"address" : "ConcertAddress" + i,
		"date" : new Date(),
		"performers" : [perfWithId._id],
		"tickets" : [i, 1000000 - i]
	};
	db.concerts.insert(concert);
}
db.concerts.ensureIndex({"title":1}, {unique:true});
db.performers.ensureIndex({"title":1}, {unique:true});



